using System;

using Microsoft.EntityFrameworkCore;

namespace MvcPomeloPkMigration.Data
{
    public class AppDbContext : DbContext {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {}
        public DbSet<MyBook> Books { get; set; }
    }
}