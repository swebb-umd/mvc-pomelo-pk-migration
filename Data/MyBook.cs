using System;

using System.ComponentModel.DataAnnotations.Schema;

namespace MvcPomeloPkMigration.Data
{
    [Table("MyBooks")]
    public class MyBook {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}