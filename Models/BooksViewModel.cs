using System;
using System.Collections.Generic;

using MvcPomeloPkMigration.Data;

namespace MvcPomeloPkMigration.Models
{
    public class BooksViewModel
    {
        public IList<MyBook> AllBooks { get; set; }

        public string BooksTableName { get; set; }
    }
}
