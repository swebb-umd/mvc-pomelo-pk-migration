﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MvcPomeloPkMigration.Migrations
{
    public partial class RenameToMyBooks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Books",
                table: "Books");

            migrationBuilder.RenameTable(
                name: "Books",
                newName: "MyBooks");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MyBooks",
                table: "MyBooks",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_MyBooks",
                table: "MyBooks");

            migrationBuilder.RenameTable(
                name: "MyBooks",
                newName: "Books");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Books",
                table: "Books",
                column: "Id");
        }
    }
}
